<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OlqrController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/save', [OlqrController::class, 'save']);

Route::middleware(['cors'])->group(function () {
    Route::get('/get-data', [OlqrController::class, 'getPath']);
    Route::get('/get-content', [OlqrController::class, 'getContent']);
    Route::get('/isupload/{order}', [OlqrController::class, 'isuploadDone']);
    Route::get('/delete/{order}', [OlqrController::class, 'deleteOrder']);
});


Route::get('/test-data', [OlqrController::class, 'test']);
