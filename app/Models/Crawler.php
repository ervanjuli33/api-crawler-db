<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crawler extends Model
{
    use HasFactory;

    protected $table = 'crawler';
    protected $primary = 'id';
    protected $fillable = ['path', 'description', 'url', 'title', 'isupload', 'order', 'jenis', 'content'];
}
