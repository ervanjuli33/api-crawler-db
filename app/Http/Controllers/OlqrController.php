<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Crawler;
use Illuminate\Support\Facades\DB;

class OlqrController extends Controller
{
    public function save(Request $request){
        try {
            $crawler = $request->all();
            DB::beginTransaction();
            foreach ($crawler['crawler'] as $item) {
                $data = [
                    'path' => $item['path'],
                    'content' => $item['content'],
                    'description' => $item['description'],
                    'url' => $item['url'],
                    'title' => $item['title'],
                    'isupload' => $item['isupload'],
                    'order' => $item['order'],
                    'jenis' => $item['jenis'],
                ];

                Crawler::query()->create($data);
            }
            DB::commit();
            return 'Success';
        }catch (\Exception $exception){
            DB::rollBack();
            return 'Failed: '.$exception->getMessage();
        }
    }

    public function getPath(){
        $data = Crawler::query()->get();

        $result = [];
        foreach ($data as $key => $value) {
            $result[$value->url] = true;
        }

        $max = Crawler::query()->max('order');

        return ['data' => $result, 'order' => $max ?? 0];
    }

    public function test(){
        try {
            $data = [
                'path' => '/olqr',
                'description' => '<p>Halaman Index</p>',
                'url' => 'http://localhots/olqr/nav',
                'title' => 'Halaman Index',
                'isupload' => true,
                'order' => 0,
                'jenis' => 'INDEX'
            ];

            Crawler::create($data);

            return 'berhasil';
        } catch (\Exception $e) {
            //throw $th;
            return 'gagal '.$e->getMessage();
        }
    }

    public function getContent(){
        $order = Crawler::query()
            ->where('isupload', false)
            ->min('order');

        return Crawler::query()
            ->where('order', $order)
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();
    }

    public function isuploadDone($order){
        try {
            $data = Crawler::query()
                ->where('order', $order)
                ->update(['isupload' => true]);

            return 'Success update isupload';
        }catch (\Exception $e){
            return 'Failed: '.$e->getMessage();
        }
    }

    public function deleteOrder($order){
        try {
            Crawler::query()
                ->where('order', $order)
                ->delete();
            return 'Success delete order';
        }catch (\Exception $e){
            return 'Failed: '.$e->getMessage();
        }
    }
}
